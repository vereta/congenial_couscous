#include "functions.hpp"

/**
 * @brief Adds two numbers
 * 
 * @param num1 first number
 * @param num2 second number
 * @return int adding result
 */
int add(int num1, int num2) {
    return num1 + num2;
}

/**
 * @brief Substracts num2 from num1
 * 
 * @param num1 first number
 * @param num2 second number
 * @return int substraction result
 */
int subtract(int num1, int num2) {
    return num1 - num2;
}

/**
 * @brief Multiplies two numbers
 * 
 * @param num1 first number
 * @param num2 second number
 * @return int multiplication result
 */
int multiply(int num1, int num2) {
    return num1 * num2;
}

int divide(int num1, int num2) {
    return num2 ? (num1 / num2) : 0;
}

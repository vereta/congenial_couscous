#include "functions.hpp"

#define BIT(x) (1 << (x))

/*Set or unset corresponding bit in a given int number */
void setBit(int& value, size_t bitNumber, bool enabled) {
    if(enabled) {
        value |= BIT(bitNumber);
    } else {
        value &= ~BIT(bitNumber);
    }
}
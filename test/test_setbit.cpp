#include <gtest/gtest.h>
#include "functions.hpp"

/*Simple set bit */
TEST(test_setBit, test_setBit_set)
{
    int num = 5;
    setBit(num, 1, true);

    EXPECT_EQ(num, 7);
}

/*Set bit that is 1 */
TEST(test_setBit, test_setBit_set_the_same_bit)
{
    int num = 5;
    setBit(num, 0, true);

    EXPECT_EQ(num, 5);
}

/*Simple clear bit */
TEST(test_setBit, test_setBit_clear)
{
    int num = 5;
    setBit(num, 0, false);

    EXPECT_EQ(num, 4);
}

/*Clear bit that is 0 */
TEST(test_setBit, test_setBit_clear_the_same_bit)
{
    int num = 5;
    setBit(num, 1, false);

    EXPECT_EQ(num, 5);
}

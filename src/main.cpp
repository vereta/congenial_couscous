#include <array>
#include <vector>
#include "app.hpp"

template<typename T>
int do_operation(int num1, int num2, char operation, T &oper) {
    for(auto &op : oper) {
        if(op.operation == operation) {
            return op.func_t(num1, num2);
        }
    }
    return 0;
}

template<typename T>
void do_operations(int num1, int num2, T &oper) {
    for(auto &op : oper) {
        std::cout << num1 << op.operation << num2 << '=' << op.func_t(num1, num2) << std::endl;
        }
    }

int main()
{
    int num = 5; /* 0101 */
    int num1 = 10;
    int num2 = 2;

    setBit(num, 1, true); /*0111 */
    std::cout << num << std::endl;

    setBit(num, 2, false); /*0011 */
    std::cout << num << std::endl;

    std::array<operation_t, 4> operations = {{
        {'+', add},
        {'-', subtract},
        {'*', multiply},
        {'/', divide}
    }};

    std::vector<oper_t> oper = {
        {'+', add},
        {'-', subtract},
        {'*', multiply},
        {'/', divide}
    };

    int result = do_operation(num1, num2, '+', oper);
    std::cout << result << std::endl;

    do_operations(num1, num2, operations);

    return 0;
}

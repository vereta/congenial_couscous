#include <gtest/gtest.h>
#include "functions.hpp"

TEST(test_functions, test_functoins_add)
{
    EXPECT_EQ(add(11, 42), 11 + 42);
}

TEST(test_functions, test_functoins_subtract)
{
    EXPECT_EQ(subtract(42, 7), 42 - 7);
}

TEST(test_functions, test_functoins_multiply)
{
    EXPECT_EQ(multiply(42, 7), 42 * 7);
}

TEST(test_functions, test_functoins_divide)
{
    EXPECT_EQ(divide(42, 7), 42 / 7);
}

TEST(test_functions, test_functoins_divide_zero)
{
    EXPECT_EQ(divide(42, 0), 0);
}

1) create function: void setBit(int& value, size_t bitNumber, bool enabled)
which will set or unset corresponding bit in a given int number.

2) create following functions for several arithmetical operations:
int add(int, int)
int divide(int, int)
int multiply(int int)
int subtract(int, int)

using typedef or std::function to get pointer (let's name it func_t)to any of these functions:

create following structure:

struct operation_t {
    char operation;
    func_t func;
};

also create an array which matches operations with corresponding functions:
operation_t operations[] {
...<insert your values here>
};

then iterate over this array and call each function with given arguments and print the results
#pragma once

#include <iostream>
#include <functional>

/*Set or unset corresponding bit in a given int number */
void setBit(int& value, size_t bitNumber, bool enabled);

int add(int num1, int num2);
int subtract(int num1, int num2);
int multiply(int num1, int num2);
int divide(int num1, int num2);

typedef int (*f_ptr)(int, int);

/* C++ functional style */
struct operation_t {
    char                            operation;
    std::function<int(int, int)>    func_t;
};

/* Old-school style */
struct oper_t {
    char                            operation;
    f_ptr                           func_t;
};
